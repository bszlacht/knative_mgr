Pillow==9.0.1
opencv-python-headless==4.9.0.80
numpy==1.26.3
scipy==1.11.4
pika==1.3.2