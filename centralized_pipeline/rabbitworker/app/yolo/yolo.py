# YOLO object detection

import cv2 as cv
import numpy as np
import os

# Get the directory where this script is located
current_dir = os.path.dirname(os.path.abspath(__file__))

# Construct the full path to coco.names
coco_path = os.path.join(current_dir, 'coco.names')
config_path = os.path.join(current_dir, 'yolov3.cfg')
weights_path = os.path.join(current_dir, 'yolov3.weights')
WHITE = (255, 255, 255)
img = None
outputs = None
IMG_SIZE = (416, 416)

# Load names of classes and get random colors for the detected areas
classes = open(coco_path).read().strip().split('\n')
np.random.seed(42)
colors = np.random.randint(0, 255, size=(len(classes), 3), dtype='uint8')

# Give the configuration and weight files for the model and load the network.
net = cv.dnn.readNetFromDarknet(config_path, weights_path)
net.setPreferableBackend(cv.dnn.DNN_BACKEND_OPENCV)
# net.setPreferableTarget(cv.dnn.DNN_TARGET_CPU)

# determine the output layer
ln = net.getLayerNames()
ln = [ln[i - 1] for i in net.getUnconnectedOutLayers()]


def load_image(image: np.ndarray):
    global img, outputs, ln
    img = image
    blob = cv.dnn.blobFromImage(img, 1 / 255.0, IMG_SIZE, swapRB=True, crop=False)

    net.setInput(blob)
    outputs = net.forward(ln)

    outputs = np.vstack(outputs)

    post_process(img, outputs, 0.5)
    # cv.waitKey(0)
    # cv.imwrite('output.jpg', img)
    return img


def post_process(img, outputs, conf):
    H, W = img.shape[:2]

    boxes = []
    confidences = []
    classIDs = []

    for output in outputs:
        scores = output[5:]
        classID = np.argmax(scores)
        confidence = scores[classID]
        if confidence > conf:
            x, y, w, h = output[:4] * np.array([W, H, W, H])
            p0 = int(x - w // 2), int(y - h // 2)
            p1 = int(x + w // 2), int(y + h // 2)
            boxes.append([*p0, int(w), int(h)])
            confidences.append(float(confidence))
            classIDs.append(classID)
            print(classID)
            # cv.rectangle(img, p0, p1, WHITE, 1)

    indices = cv.dnn.NMSBoxes(boxes, confidences, conf, conf - 0.1)
    if len(indices) > 0:
        for i in indices.flatten():
            (x, y) = (boxes[i][0], boxes[i][1])
            (w, h) = (boxes[i][2], boxes[i][3])
            color = [int(c) for c in colors[classIDs[i]]]
            cv.rectangle(img, (x, y), (x + w, y + h), color, 2)
            text = "{}: {:.4f}".format(classes[classIDs[i]], confidences[i])
            cv.putText(img, text, (x, y - 5), cv.FONT_HERSHEY_SIMPLEX, 0.5, color, 1)

# path = 'images/horse.jpg'
# img = cv.imread(path)
# img = cv.resize(img, IMG_SIZE)
# load_image(img)
# cv.destroyAllWindows()
