import base64
import json
import os
from io import BytesIO

import numpy as np
from PIL import Image
from scipy import ndimage
import pika

import yolo

RESIZE_WIDTH = 416
RESIZE_HEIGHT = 416

RABBIT_MQ_HOST = os.getenv("RABBITMQ_HOST")
QUEUE_NAME = os.getenv("QUEUE_NAME")
RABBIT_MQ_USERNAME = os.getenv("RABBITMQ_USERNAME")
RABBIT_MQ_PASSWORD = os.getenv("RABBITMQ_PASSWORD")

credentials = pika.PlainCredentials(RABBIT_MQ_USERNAME, RABBIT_MQ_PASSWORD)


def yolo_fun(image_bytes):
    image = Image.open(BytesIO(image_bytes))
    np_image = np.array(image)
    processed_image = yolo.load_image(np_image)
    img = Image.fromarray(processed_image)
    img_byte_arr = BytesIO()
    img.save(img_byte_arr, format='PNG')
    return base64.b64encode(img_byte_arr.getvalue()).decode('utf-8')


def resize(image_bytes):
    image = Image.open(BytesIO(image_bytes))
    resized_image = image.resize((RESIZE_WIDTH, RESIZE_HEIGHT))
    img_byte_arr = BytesIO()
    resized_image.save(img_byte_arr, format='PNG')
    return base64.b64encode(img_byte_arr.getvalue()).decode('utf-8')


def correct_quality(image_bytes):
    image = Image.open(BytesIO(image_bytes))
    img_array = np.array(image)
    filtered_img_array = ndimage.median_filter(img_array, size=2)
    filtered_image = Image.fromarray(filtered_img_array)
    img_byte_arr = BytesIO()
    filtered_image.save(img_byte_arr, format='PNG')
    return base64.b64encode(img_byte_arr.getvalue()).decode('utf-8')


# rabbitmq functions

def publish_message(queue_name, message, id: str):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=RABBIT_MQ_HOST, credentials=credentials))
    channel = connection.channel()
    channel.queue_declare(queue=QUEUE_NAME, durable=True)
    message_data = {
        'id': id,
        'image': message
    }
    channel.basic_publish(exchange='', routing_key=queue_name, body=json.dumps(
        message_data), properties=pika.BasicProperties(delivery_mode=2))
    print(f"Sent: {message}")
    connection.close()


def consume():
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=RABBIT_MQ_HOST, credentials=credentials))
    channel = connection.channel()
    channel.queue_declare(queue=QUEUE_NAME, durable=True)

    def callback(ch, method, properties, body):
        message_data = json.loads(body)
        identifier = message_data.get('id')
        image = message_data.get('image')
        try:
            if identifier == "1":
                publish_message(QUEUE_NAME, image, '2')
            elif identifier == "2":
                publish_message(QUEUE_NAME, image, '3')
            elif identifier == "3":
                print("Processing from 3...")

            # Acknowledge the message after processing
            ch.basic_ack(delivery_tag=method.delivery_tag)
        except Exception as e:
            print(f"Failed to process message: {e}")
            # Optionally you can handle re-queueing or logging here

    channel.basic_consume(
        queue=QUEUE_NAME, on_message_callback=callback, auto_ack=False)

    print('Waiting for messages. To exit press CTRL+C')
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        print('Exiting...')
    finally:
        connection.close()


if __name__ == "__main__":
    consume()
