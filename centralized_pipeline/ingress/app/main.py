import json
import os
import pika
from flask import Flask, request, jsonify

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 10 * 1024 * 1024  # Limit to 10 MB


RABBIT_MQ_HOST = os.getenv("RABBITMQ_HOST")
QUEUE_NAME = os.getenv("QUEUE_NAME")
RABBIT_MQ_USERNAME = os.getenv("RABBITMQ_USERNAME")
RABBIT_MQ_PASSWORD = os.getenv("RABBITMQ_PASSWORD")

credentials = pika.PlainCredentials(RABBIT_MQ_USERNAME, RABBIT_MQ_PASSWORD)


def publish_message(queue, message):
    try:
        # Create a new connection and channel for each publish
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=RABBIT_MQ_HOST, credentials=credentials))
        channel = connection.channel()
        channel.queue_declare(queue=queue, durable=True)

        message_data = {
            'id': "1",
            'image': message
        }
        channel.basic_publish(exchange='', routing_key=queue, body=json.dumps(message_data),
                              properties=pika.BasicProperties(delivery_mode=2))
        print(f"Message sent to {queue}: {message}")
        return f"Message sent successfully to {queue}"
    except Exception as e:
        return f"An unexpected error occurred: {e}"
    finally:
        if 'channel' in locals() and channel.is_open:
            channel.close()
        if 'connection' in locals() and connection.is_open:
            connection.close()


@app.route('/publish', methods=['POST'])
def publish():
    data = request.get_json()
    if not data or 'topic' not in data or 'image' not in data:
        return jsonify({"error": "Invalid request, 'topic' and 'image' are required."}), 400

    # This can be used to decide the queue if you have multiple queues
    topic = data['topic']
    base64_image = data['image']

    # Publish the message
    result = publish_message(QUEUE_NAME, base64_image)

    return jsonify({"status": result}), 200



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
