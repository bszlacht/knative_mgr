import os
from flask import Flask, jsonify
from prometheus_api_client import PrometheusConnect
import datetime

app = Flask(__name__)

PROMETHEUS_URL = os.getenv("PROMETHEUS_URL")

# Initialize the Prometheus client
prom = PrometheusConnect(url=PROMETHEUS_URL, disable_ssl=True)

def get_node_metrics():
    end_time = datetime.datetime.now()
    start_time = end_time - datetime.timedelta(minutes=1)

    # Query for CPU idle rate over the past minute
    cpu_query = 'sum by (node) (rate(node_cpu_seconds_total{mode="idle"}[2m])) / sum by (node) (rate(node_cpu_seconds_total{}[2m]))'
    
    # Query for memory usage percentage
    memory_query = '(1 - (node_memory_MemAvailable_bytes / node_memory_MemTotal_bytes)) * 100'
    
    # Query for renewable energy percentage
    renewable_energy_query = 'renewable_energy_percentage'

    cpu_usage = prom.custom_query_range(
        query=cpu_query,
        start_time=start_time,
        end_time=end_time,
        step="2m"
    )

    memory_usage = prom.custom_query_range(
        query=memory_query,
        start_time=start_time,
        end_time=end_time,
        step="2m"
    )

    renewable_energy_usage = prom.custom_query_range(
        query=renewable_energy_query,
        start_time=start_time,
        end_time=end_time,
        step="2m"
    )

    # Format the response data
    metrics = []

    for cpu_result, mem_result, energy_result in zip(cpu_usage, memory_usage, renewable_energy_usage):
        node = cpu_result['metric']['node']  # Extract node
        cpu_idle_rate = float(cpu_result['values'][-1][1])  # CPU idle rate
        mem_used_percentage = float(mem_result['values'][-1][1])  # Memory usage percentage
        renewable_energy_percentage = float(energy_result['values'][-1][1])  # Renewable energy percentage

        metrics.append({
            'node': node,
            'cpu_idle_rate': cpu_idle_rate,
            'memory_used_percentage': mem_used_percentage,
            'renewable_energy_percentage': renewable_energy_percentage
        })

    return metrics

@app.route('/node_metrics', methods=['GET'])
def node_metrics():
    metrics = get_node_metrics()
    return jsonify(metrics)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
