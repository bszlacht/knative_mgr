import random
import time
from prometheus_client import start_http_server, Gauge
from flask import Flask

app = Flask(__name__)

# Custom Gauge metric for renewable energy percentage per node
renewable_energy_gauge = Gauge('renewable_energy_percentage', 'Percentage of energy from renewable sources', ['node'])

# Dictionary to track renewable energy percentage for each node
node_renewable_energy = {}

# Function to simulate and update renewable energy percentage for each node
def update_renewable_energy_percentage():
    nodes = ['kafkanode', 'node2', 'node3']  # Define your nodes
    for node in nodes:
        if node not in node_renewable_energy:
            node_renewable_energy[node] = random.randint(30, 70)  # Initialize with random value

        # Simulate a change in renewable energy percentage (random fluctuation)
        change = random.randint(-3, 3)
        new_value = node_renewable_energy[node] + change
        node_renewable_energy[node] = max(0, min(new_value, 100))  # Keep within range [0, 100]

        # Update the Prometheus gauge for this node
        renewable_energy_gauge.labels(node=node).set(node_renewable_energy[node])

# Flask route (optional) to return current energy percentages (for visibility)
@app.route('/renewable_energy', methods=['GET'])
def renewable_energy():
    return {node: energy for node, energy in node_renewable_energy.items()}

if __name__ == '__main__':
    # Start up the Prometheus HTTP server to expose the metrics
    start_http_server(8000)  # Prometheus will scrape from port 8000

    # Update metrics in a loop
    while True:
        update_renewable_energy_percentage()
        time.sleep(60)  # Update every 2 minutes
