/opt/bitnami/kafka/bin/kafka-topics.sh --create \
  --bootstrap-server localhost:9092 \
  --command-config /opt/bitnami/kafka/config/sasl_client_config.properties \
  --topic t1 \
  --partitions 1 \
  --replication-factor 1

  