import base64
from io import BytesIO

from PIL import Image


def image_to_base64(image_path):
    # Open the image file
    with Image.open(image_path) as img:
        # Convert image to BytesIO
        buffered = BytesIO()
        img.save(buffered, format="PNG")

        # Encode the image to base64
        img_base64 = base64.b64encode(buffered.getvalue()).decode('utf-8')

    return img_base64


def save_base64_to_file(base64_string, output_file):
    with open(output_file, "w") as file:
        file.write(base64_string)


# Example usage:
image_path = 'img_11zon.jpg'  # Replace with your image path
output_txt_path = 'output_image.txt'  # Path to save the base64 string

# Convert the image to base64
base64_image = image_to_base64(image_path)

# Save the base64 string to a .txt file
save_base64_to_file(base64_image, output_txt_path)

print(f"Base64 image string saved to {output_txt_path}")
